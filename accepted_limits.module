<?php

/**
 * @todo Add formatting and/or message when viewing the node.
 */

define('ACCEPTED_LIMITS_MIN_MESSAGE', 'The number you entered for !field is smaller than the accepted lower limit of !accepted_min.');
define('ACCEPTED_LIMITS_MAX_MESSAGE', 'The number you entered for !field is larger than the accepted upper limit of !accepted_max.');

/**
 * Implementation of hook_help().
 */
function accepted_limits_help($path, $arg) {
  switch ($path) {
    case 'admin/help#accepted_limits':
      return t('Set accepted limits for CCK number fields with custom validation messages.');
  }
}

/**
 * Implementation of hook_field_settings_alter().
 */
function accepted_limits_field_settings_alter(&$form, $op, $field) {
  // Don't do anything if field is not a number.
  if (!in_array($field['type'], array('number_integer', 'number_decimal', 'number_float'))) {
    return;
  }
  switch ($op) {
    case 'form':
      $replacements = array(
        '!field' => $field['widget']['label'],
        '!accepted_min' => $field['accepted_limit_min'],
        '!accepted_max' => $field['accepted_limit_max'],
      );
      $form['accepted_limits'] = array(
        '#type' => 'fieldset',
        '#title' => t('Accepted limits'),
        '#description' => t('Define the accepted limits for this field and set custom messages if the value is outside of these limits.') . '<br />' . t('Allowed replacement patterns for messages are<ul><li><strong>!field</strong>: field name</li><li><strong>!value</strong>: entered value</li><li><strong>!accepted_min</strong>: accepted minimum limit</li><li><strong>!accepted_max</strong>: accepted maximum limit</li></ul>'),
        '#collapsible' => TRUE,
        '#collapsed' => empty($field['accepted_limit_max']) && empty($field['accepted_limit_min']) ? TRUE : FALSE,
      );
      $form['accepted_limits']['accepted_limit_min'] = array(
        '#type' => 'textfield',
        '#title' => t('Minimum'),
        '#default_value' => isset($field['accepted_limit_min']) ? $field['accepted_limit_min'] : '',
        '#element_validate' => array('_accepted_limits_field_settings_validate'),
        '#weight' => -10,
      );
      $form['accepted_limits']['accepted_limit_min_message'] = array(
        '#type' => 'textfield',
        '#title' => t('Message when exceeding minimum limit'),
        '#default_value' => isset($field['accepted_limit_min_message']) ? $field['accepted_limit_min_message'] : '',
        '#description' => t('If no message is entered, the default will be used: ') . '<strong>' . t(ACCEPTED_LIMITS_MIN_MESSAGE, $replacements) . '</strong>',
        '#weight' => -9,
      );
      $form['accepted_limits']['accepted_limit_max'] = array(
        '#type' => 'textfield',
        '#title' => t('Maximum'),
        '#default_value' => isset($field['accepted_limit_max']) ? $field['accepted_limit_max'] : '',
        '#element_validate' => array('_accepted_limits_field_settings_validate'),
        '#weight' => -8,
      );
      $form['accepted_limits']['accepted_limit_max_message'] = array(
        '#type' => 'textfield',
        '#title' => t('Message when exceeding maximum limit'),
        '#default_value' => isset($field['accepted_limit_max_message']) ? $field['accepted_limit_max_message'] : '',
        '#description' => t('If no message is entered, the default will be used: ') . '<strong>' . t(ACCEPTED_LIMITS_MAX_MESSAGE, $replacements) . '</strong>',
        '#weight' => -7,
      );
      $form['accepted_limits']['accepted_limit_display'] = array(
        '#type' => 'checkboxes',
        '#title' => t('Message display'),
        '#description' => t('Specify when to display the warning messages.'),
        '#default_value' => isset($field['accepted_limit_display']) ? $field['accepted_limit_display'] : array('submit', 'edit'),
        '#options' => array(
          'submit' => t('When submitting'),
          'edit' => t('When editing'),
        ),
      );
      break;
    case 'save':
      $form = array_merge($form, array('accepted_limit_max', 'accepted_limit_max_message', 'accepted_limit_min', 'accepted_limit_min_message', 'accepted_limit_display'));
      break;
  }
}

/**
 * Make sure the accepted limits are not beyond the actual max/min of the field.
 */
function _accepted_limits_field_settings_validate($element, &$form_state) {
  switch ($element['#name']) {
    case 'accepted_limit_max':
      if (is_numeric($form_state['values']['accepted_limit_max'])
      && is_numeric($form_state['values']['max'])
      && $form_state['values']['accepted_limit_max'] >= $form_state['values']['max']) {
        form_set_error('accepted_limit_max', t('The accepted maximum has to be lower than the absolute maximum.'));
      }
      elseif (is_numeric($form_state['values']['accepted_limit_max'])
      && is_numeric($form_state['values']['accepted_limit_min'])
      && $form_state['values']['accepted_limit_min'] >= $form_state['values']['accepted_limit_max']) {
        form_set_error('accepted_limit_max', t('The accepted maximum has to be higher than the accepted minimum.'));
      }
      break;
    case 'accepted_limit_min':
      if (is_numeric($form_state['values']['accepted_limit_min'])
      && is_numeric($form_state['values']['min'])
      && $form_state['values']['accepted_limit_min'] <= $form_state['values']['min']) {
        form_set_error('accepted_limit_min', t('The accepted minimum has to be higher than the absolute minimum.'));
      }
      break;
  }
}

/**
 * Implementation of hook_elements().
 */
function accepted_limits_elements() {
  return array(
    'number' => array(
      '#process' => array('accepted_limits_element_process'),
      '#element_validate' => array('accepted_limits_element_validate'),
      '#accepted_limits' => FALSE,
    ),
  );
}

/**
 * Form element process function to add accepted limits information.
 */
function accepted_limits_element_process($element, $edit, &$form_state, $form) {
  // Do nothing if it's not a node editing form.
  if ($form['#id'] != 'node-form') {
    return $element;
  }
  if (empty($element['#accepted_limits']) && isset($form['#field_info'])
  && ($form['#field_info'][$element['#field_name']]['accepted_limit_max'] != ''
  || $form['#field_info'][$element['#field_name']]['accepted_limit_min'] != '')) {
    $element['#accepted_limits'] = array(
      'accepted_limit_max' => $form['#field_info'][$element['#field_name']]['accepted_limit_max'],
      'accepted_limit_max_message' => $form['#field_info'][$element['#field_name']]['accepted_limit_max_message'],
      'accepted_limit_min' => $form['#field_info'][$element['#field_name']]['accepted_limit_min'],
      'accepted_limit_min_message' => $form['#field_info'][$element['#field_name']]['accepted_limit_min_message'],
      'accepted_limit_display' => $form['#field_info'][$element['#field_name']]['accepted_limit_display'],
    );
  }
  // Set message even while editing the node.
  if (!empty($element['#accepted_limits']['accepted_limit_display']['edit'])) {
    if ($element['#value']['value'] != '' && ($element['#accepted_limits']['accepted_limit_max'] != '' || $element['#accepted_limits']['accepted_limit_min'] != '')) {
      if ($element['#value']['value'] > $element['#accepted_limits']['accepted_limit_max']) {
        _accepted_limits_set_message($element, 'max');
      }
      elseif ($element['#value']['value'] < $element['#accepted_limits']['accepted_limit_min']) {
        _accepted_limits_set_message($element, 'min');
      }
    }
  }
  return $element;
}

/**
 * Display a warning if the entered value is outside of the accepted limits.
 */
function accepted_limits_element_validate($element, &$form_state) {
  // Don't try to validate if there are no accepted limits set or if it's configured not to display at submission.
  if ($element['#accepted_limits'] == FALSE || empty($element['#accepted_limits']['accepted_limit_display']['submit'])) {
    return;
  }
  if ($element['#accepted_limits']['accepted_limit_max'] != '') {
    if ($form_state['values'][$element['#field_name']][$element['#delta']]['value'] != ''
    && $form_state['values'][$element['#field_name']][$element['#delta']]['value'] > $element['#accepted_limits']['accepted_limit_max']) {
      _accepted_limits_set_message($element, 'max');
    }
  }
  if ($element['#accepted_limits']['accepted_limit_min'] != '') {
    if ($form_state['values'][$element['#field_name']][$element['#delta']]['value'] != ''
    && $form_state['values'][$element['#field_name']][$element['#delta']]['value'] < $element['#accepted_limits']['accepted_limit_min']) {
      _accepted_limits_set_message($element, 'min');
    }
  }
}

/**
 * Display the corresponding message.
 */
function _accepted_limits_set_message(&$element, $op) {
  $message = '';
  $replacements = array(
    '!field' => $element['#title'],
    '!value' => $element['#value']['value'],
    '!accepted_min' => $element['#accepted_limits']['accepted_limit_min'],
    '!accepted_max' => $element['#accepted_limits']['accepted_limit_max'],
  );
  switch ($op) {
    case 'max':
      if (empty($element['#accepted_limits']['accepted_limit_max_message'])) {
        $message = ACCEPTED_LIMITS_MAX_MESSAGE;
      }
      else {
        $message = $element['#accepted_limits']['accepted_limit_max_message'];
      }
      break;
    case 'min':
      if (empty($element['#accepted_limits']['accepted_limit_min_message'])) {
        $message = ACCEPTED_LIMITS_MIN_MESSAGE;
      }
      else {
        $message = $element['#accepted_limits']['accepted_limit_min_message'];
      }
      break;
  }
  drupal_set_message(t($message, $replacements), 'warning', FALSE);
}
